class CreateUsers < ActiveRecord::Migration
  def change
    create_table(:users) do |t|
      t.string :name
      t.string :email
      t.string :authentication_token
      t.string :fb_id
      t.string :fb_token
      t.string :image_url
      t.string :birth_date
      t.string :gender
      t.string :location

      t.timestamps
    end
  end
end
