require "rails_helper"

RSpec.describe "Errors API", type: :request do
  context "Returns logged user" do
    describe "GET #routing" do
      let!(:user) { create(:user) }
      before do
        get "/undefined_route", {}, {}
      end

      it "returns a unsuccessful response" do
        expect(response).to_not be_success
      end

      it "returns a user" do
        expect(JSON.parse(response.body)).to eq(
          "error" => {
            "message" => "Endpoint requested not found.",
            "class" => "RoutingError",
            "status" => 404
          })
      end
    end
  end
end
