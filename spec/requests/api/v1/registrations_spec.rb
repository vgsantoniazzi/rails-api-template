require "rails_helper"

RSpec.describe "Api::V1::Registration API", type: :request do
  context "Create new user" do
    describe "POST #sign_up" do
      let!(:user) { build(:user) }
      before do
        post "/api/v1/registrations/sign_up", { user: user.attributes } , {}
      end

      it "returns a successful response" do
        expect(response).to be_success
      end

      it "creates a new user" do
        expect(User.count).to eq(1)
      end

      it "returns a user" do
        user = User.first!
        expect(JSON.parse(response.body)).to eq(
          "user" => {
            "name" => user.name,
            "email" => user.email,
            "fb_id" => user.fb_id,
            "fb_token" => user.fb_token,
            "image_url" => user.image_url,
            "birth_date" => user.birth_date,
            "gender" => user.gender,
            "location" => user.location,
            "authentication_token" => user.authentication_token
          }
        )
      end
    end
    describe "POST #sign_up" do
      let!(:user) { create(:user) }
      before do
        post "/api/v1/registrations/sign_up",
             { user: user.attributes.merge(image_url: "google.com/Osd") } , {}
      end

      it "returns a successful response" do
        expect(response).to be_success
      end

      it "creates a new user" do
        expect(User.count).to eq(1)
      end

      it "returns a user" do
        user = User.first!
        expect(JSON.parse(response.body)).to eq(
          "user" => {
            "name" => user.name,
            "email" => user.email,
            "fb_id" => user.fb_id,
            "fb_token" => user.fb_token,
            "image_url" => user.image_url,
            "birth_date" => user.birth_date,
            "gender" => user.gender,
            "location" => user.location,
            "authentication_token" => user.authentication_token
          }
        )
      end

      it "update user" do
        expect(User.first.image_url).to eq("google.com/Osd")
      end
    end
  end
  context "Try create user without email" do
    describe "POST #sign_up" do
      let!(:user) { build(:user) }
      before do
        post "/api/v1/registrations/sign_up",
             { user: user.attributes.merge(email: nil) }, {}
      end

      it "returns a successful response" do
        expect(response).to be_success
      end

      it "doesn't create a new user" do
        expect(User.count).to eq(0)
      end

      it "returns a user" do
        expect(JSON.parse(response.body)).to eq(
          "error" => {
            "message" => {
              "email" => ["can't be blank"]
            },
            "class" => "ModelValidationError",
            "status" => 201
          })
      end
    end
  end
end
