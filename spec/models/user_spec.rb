# == Schema Information
#
# Table name: users
#
#  id                   :integer          not null, primary key
#  name                 :string
#  email                :string
#  authentication_token :string
#  fb_id                :string
#  fb_token             :string
#  image_url            :string
#  birth_date           :string
#  gender               :string
#  location             :string
#  created_at           :datetime
#  updated_at           :datetime
#

require "rails_helper"

RSpec.describe User, type: :model do
  context "Validations" do
    it { should validate_presence_of :email }
    it { should validate_presence_of :name }
    it { should validate_presence_of :fb_id }
    it { should validate_presence_of :fb_token }
    it { should validate_presence_of :image_url }
    it { should validate_presence_of :birth_date }
    it { should validate_presence_of :gender }
    it { should validate_presence_of :location }

    it { should validate_uniqueness_of :email }
    it { should validate_uniqueness_of :fb_id }
    it { should validate_uniqueness_of :fb_token }
  end

  context "Callbacks" do
    describe "#authentication_token" do
      let!(:user) { build(:user) }

      before do
        expect(user.authentication_token).to be_nil
        user.save!
      end

      it "generate secure random to access token" do
        expect(user.authentication_token).to_not be_nil
      end
    end
  end
end
