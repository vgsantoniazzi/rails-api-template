# == Schema Information
#
# Table name: users
#
#  id                   :integer          not null, primary key
#  name                 :string
#  email                :string
#  authentication_token :string
#  fb_id                :string
#  fb_token             :string
#  image_url            :string
#  birth_date           :string
#  gender               :string
#  location             :string
#  created_at           :datetime
#  updated_at           :datetime
#

FactoryGirl.define do
  factory :user do
    name "User"
    sequence(:email) { |n| "user#{n}@example.com" }
    sequence(:fb_id) { |n| "#{n}1236#{n}234#{n}" }
    sequence(:fb_token) { |n| "#{n}AEcd#{n}234ffde#{n}" }
    image_url "http://aws.amazon/image"
    birth_date "13/06/1994"
    gender "Male"
    location "Pelotas, RS, BR"
  end
end
