class API::V1::BaseController < ApplicationController
  include ActionController::HttpAuthentication::Token

  attr_accessor :current_user

  before_filter :authenticate_user_from_token!

  rescue_from ApplicationError do |e|
    @status, @class, @message = e.status, e.class, parse(e.message)
    render_template "layouts/error", status: e.status
  end

  def render_template(path, status: 200)
    render "api/v1/#{path}", status: status
  end

  helper_method :current_user

  private

  def parse(message)
    begin
      return JSON.parse(message)
    rescue JSON::ParserError
      return message
    end
  end

  def authenticate_user_from_token!
    if user = User.find_by(authentication_token: token_and_options(request))
      @current_user = user
    else
      fail UnauthenticatedError.new
    end
  end
end
