class Api::V1::RegistrationsController < API::V1::BaseController
  skip_before_filter :authenticate_user_from_token!

  before_action :set_user

  def sign_up
    @user.assign_attributes(user_params)
    if @user.save
      render_template "users/show", status: 201
    else
      fail ModelValidationError.new(@user.errors)
    end
  end

  private

  def set_user
    @user ||= User.where(
      fb_id: user_params[:fb_id], fb_token: user_params[:fb_token]
    ).first_or_initialize
  end

  def user_params
    params.require(:user).permit(:name, :email, :fb_id, :fb_token, :image_url,
                                 :birth_date, :gender, :location)
  end
end
