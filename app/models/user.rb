# == Schema Information
#
# Table name: users
#
#  id                   :integer          not null, primary key
#  name                 :string
#  email                :string
#  authentication_token :string
#  fb_id                :string
#  fb_token             :string
#  image_url            :string
#  birth_date           :string
#  gender               :string
#  location             :string
#  created_at           :datetime
#  updated_at           :datetime
#

class User < ActiveRecord::Base
  before_create :generate_authentication_token

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  validates :fb_id, presence: true, uniqueness: true
  validates :fb_token, presence: true, uniqueness: true
  validates :image_url, presence: true
  validates :birth_date, presence: true
  validates :gender, presence: true
  validates :location, presence: true

  private

  def generate_authentication_token
    self.authentication_token = SecureRandom.hex
  end
end
