class UnauthenticatedError < ApplicationError
  def initialize
    @status = 401
    @class = :UnauthenticatedError
    super("Your access info aren't correct.")
  end
end
