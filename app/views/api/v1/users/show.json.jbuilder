json.user do
  json.name @user.name
  json.email @user.email
  json.fb_id @user.fb_id
  json.fb_token @user.fb_token
  json.image_url @user.image_url
  json.birth_date @user.birth_date
  json.gender @user.gender
  json.location @user.location
  json.authentication_token @user.authentication_token
end
